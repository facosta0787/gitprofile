import Home from '../src/containers/Home'
export default Home

export async function getServerSideProps (ctx) {
  const resoponse = await fetch('https://jsonplaceholder.typicode.com/users')
  const users = await resoponse.json()

  return {
    props: {
      users
    }
  }
}
