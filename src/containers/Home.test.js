import { render, screen } from '@testing-library/react'
import Home from './Home'

describe('Home Container', () => {
  test('Should render with no problem', () => {
    const { baseElement } = render(<Home />)
    expect(baseElement).toMatchSnapshot()
  })

  test('Should render the main links', () => {
    render(<Home />)
    expect(screen.getAllByTestId('card').length).toEqual(4)
  })
})
